#!/usr/bin/env bash
cd /opt/jekyll && jekyll serve --host 0.0.0.0 >> /opt/logs/jekyll.out.log
