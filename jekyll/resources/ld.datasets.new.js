function AddDataset()
{
	var title 			= $('#title').val();
	var wfsURL 			= $('#wfsURL').val();
	var description 	= $('#description').val();
	var tags			= $('#tags').val();
	
	$.post( "http://data.test.pdok.nl/api/datasets/", { title: title, wfsURL: wfsURL, description: description, tags: tags })
	.done(function( data ) 
	{
		//$("#response").html(JSON.stringify(data));
		
		$("#title").removeClass("error");
		$("#wfsURL").removeClass("error");
		
		$("#titleerror").slideUp();
		$("#wfsURLerror").slideUp();
		
		$("#succes").slideDown();
	}
	)
	.fail( function(xhr, textStatus, errorThrown) 
	{
		//$("#response").html(xhr.responseText);
		var response = JSON.parse(xhr.responseText);

		var errors = response.errors;
		
		for(error in response.errors)
		{
			//alert(error)
			$("#" + error).addClass("error");
			$("#" + error + "error").slideDown();
		}
    }
	);
}
