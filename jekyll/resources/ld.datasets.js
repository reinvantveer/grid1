
/* ng-infinite-scroll - v1.0.0 - 2013-02-23 */
var mod;mod=angular.module("infinite-scroll",[]),mod.directive("infiniteScroll",["$rootScope","$window","$timeout",function(i,n,e){return{link:function(t,l,o){var r,c,f,a;return n=angular.element(n),f=0,null!=o.infiniteScrollDistance&&t.$watch(o.infiniteScrollDistance,function(i){return f=parseInt(i,10)}),a=!0,r=!1,null!=o.infiniteScrollDisabled&&t.$watch(o.infiniteScrollDisabled,function(i){return a=!i,a&&r?(r=!1,c()):void 0}),c=function(){var e,c,u,d;return d=n.height()+n.scrollTop(),e=l.offset().top+l.height(),c=e-d,u=n.height()*f>=c,u&&a?i.$$phase?t.$eval(o.infiniteScroll):t.$apply(o.infiniteScroll):u?r=!0:void 0},n.on("scroll",c),t.$on("$destroy",function(){return n.off("scroll",c)}),e(function(){return o.infiniteScrollImmediateCheck?t.$eval(o.infiniteScrollImmediateCheck)?c():void 0:c()},0)}}}]);

var datasetApp = angular.module('datasetsApp', ['infinite-scroll']);

//Indicates if infite-scroll is loading
var loadingMore = false;

//Current counter of loaded datasets
var counter = 0;

//Array of json objects containing all datasets information (WHOLE LIST)
var datasets = [];

//Array of dataset object (WHOLE LIST AFTER SEARCH)
var datasetsSearched = [];

//Timeout object which fires a search function
var loadTimer;

datasetApp.controller('datasetsCtrl', function($scope, $http, $timeout, $filter) 
{
	$scope.datasets = [];
	
	//Totall size of searched list
	$scope.currentDatasetLength = function() {
        return datasetsSearched.length;
    };

	//Totall size of all datasets
	$scope.totallDatasetLength = function() {
        return datasets.length;
    };
	
	//Increment a dataset
	$scope.increment = function() 
	{
		//Add datasets
		if(counter < datasetsSearched.length)
		{
			$scope.datasets.push(datasetsSearched[counter]);
			counter++;
		}
	};
	
	//Get language 
	$http.get('resources/ld.language.nl.json').then(function(res)
	{
		$scope.language = res.data;
	});
	
	$scope.init = function () 
	{
		//datasets = res.data.datasets;
		$.getJSON( "http://data.test.pdok.nl/api/datasets/", function( data ) 
		{
			//$("#response").html(JSON.stringify(data));
			
			datasets = data._embedded;
			
			$scope.setdata();
		});
	};
	
	$scope.setdata = function() 
	{
		//Delete all dashes
		for(var i = 0; i < datasets.length; i++)
		{
			datasets[i].title = datasets[i].title.replace(/-/g," ").trim();
		}
		
		//Ontdubbel
		for(var i = 0; i < datasets.length; i++)
		{
			for(var j = i + 1; j < datasets.length; j++)
			{
				if(datasets[i].title == datasets[j].title)
				{
					datasets.splice(i, 1);
					i--;
					break;
				}
			}
		}
		
		//Sort on first letter of title
		datasets.sort(function(d1, d2)
						{
							if(d1.title < d2.title) return -1;
							if(d1.title > d2.title) return 1;
							return 0;
						});
		
		datasetsSearched = datasets;
		
		for(var i = 0; i < 8; i++)
			$scope.increment();		
	}
	
	//Fired when reaching the last metadata set
	$scope.loadMore = function() 
	{
		//If already loading -> return
		if(loadingMore)
			return;
		
		loadingMore = true;
		
		$scope.increment();
		
		loadingMore = false;
	};	
	
	//After key input event
	$scope.StartSearchQuery = function($event)
	{
		//Check if keyup is an number or character
		if(!(($event.keyCode > 47 && $event.keyCode < 91) || $event.keyCode == 8))
			return;
		
		$scope.search_loading = true;
		
		//Cancel timeout if already running
		if(loadTimer != undefined)
			$timeout.cancel(loadTimer);
		
		loadTimer = $timeout(function() 
		{
			$scope.DoSearchQuery();
		}, 250);
	}
	
	//Actually search and reset view
	$scope.DoSearchQuery = function()
	{
		if($scope.search_query == undefined)
			return;

		counter = 0;

		$scope.datasets = [];
		
		if($scope.search_query.length == 0)
			datasetsSearched = datasets;
		else
		{
			datasetsSearched = $filter('filter')(datasets, function($ds)
								{
									if($ds.title.toLowerCase().indexOf($scope.search_query.toLowerCase()) > -1 ||
										$ds.description.toLowerCase().indexOf($scope.search_query.toLowerCase()) > -1)
										return true;
									else
										return false;
								});
		}
								
		for(var i = 0; i < 8; i++)
			$scope.increment();
		
		$scope.search_loading = false;
    }
});

//Toggle dataset on
function ToggleOn($open)
{
	var id = $open.id.split("_")[0];
	$("#" + id + "_closed").hide();
	$("#" + id + "_open").show();
}

//Toggle dataset off
function ToggleOff($closed)
{
	var id = $closed.id.split("_")[0];
	$("#" + id + "_closed").show();
	$("#" + id + "_open").hide();
}