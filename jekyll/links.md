---
layout: page
title: Links
permalink: /links/
---
# PDOK Dataplatform Open Source
PDOK stelt de broncode van (vooralsnog delen van) de software die het PDOK Dataplatform maakt, als open source software ter beschikking.
De volgende onderdelen zijn al beschikbaar:
[Linked Data Theatre](https://github.com/architolk/Linked-Data-Theatre)
Het Linked Data Theatre verzorgt de menselijke interactie met Linked Data.

[wfs2swagger](https://github.com/PDOK/wfs2swagger)
WFS2Swagger is een Javascript module voor het samenstellen van een [Swagger](https://github.com/swagger-api/swagger-spec/blob/master/versions/2.0.md) API specificatie op basis van [Web Feature Service](https://nl.wikipedia.org/wiki/Web_Feature_Service) metadata
