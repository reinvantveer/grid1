---
layout: page
title: Maak een nieuwe dataset
---
<script src='{{ "/resources/ld.datasets.new.js" | prepend: site.baseurl }}'></script>
Geef in onderstaand veld het adres (inclusief http:// of https://) van het WFS endpoint op om deze als dataset aan
data.pdok.nl toe te voegen.

<form method="post" action="/adddataset">

	<div id="succes" class="succes">
		Uw dataset is geupload!
	</div>
	
	<table class="full" style="width: 100% !important; max-width: 600px;">
		<tr>
			<td>
				Titel van de dataset *
			</td>
			<td>
				<input type="text" name="title" id="title" />
			</td>
		</tr>
		<tr>
			<td colspan="2" id="titleerror" class="errortext">
				Titel veld is vereist
			</td>
		</tr>
		<tr>
			<td>
				Adres van het WFS-endpoint *
			</td>
			<td>
				<input type="text" name="wfsURL" id="wfsURL" />
			</td>
		</tr>
		<tr>
			<td colspan="2" id="wfsURLerror" class="errortext">
				WFS-endpoint veld is vereist
			</td>
		</tr>
		<tr>
			<td>
				Samenvatting van wat de dataset bevat
			</td>
			<td>
				<input type="text" name="description" id="description" />
			</td>
		</tr>
		<tr>
			<td>
				Tags (komma-gescheiden)
			</td>
			<td>
				<input type="text" name="tags" id="tags" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<i>* Vereiste velden</i>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" value="Toevoegen" onClick="AddDataset();" class="form-submit" />
			</td>
		</tr>
	</table>
	
	<div id="response">
	
	</div>
</form>

