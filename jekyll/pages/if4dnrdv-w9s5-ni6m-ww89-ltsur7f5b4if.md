---
layout: dataset
title: Vaarwegkenmerken-in-Nederland-bevaarbaarheidsinformatie
date: Mon Nov 16 2015 16:27:09 GMT+0100 (CET)
modified: 2014-12-08
tags: if4dnrdv-w9s5-ni6m-ww89-ltsur7f5b4if, Vaarwegkenmerken-in-Nederland-bevaarbaarheidsinformatie, 
---
## Toelichting: 
Bevaarbaarheid Voor iedere haven, vaarweg of een deel van een vaarweg is een zogenoemde bevaarbaarheidsklasse toegekend. De indeling voor die klassen is gebaseerd op de afmetingen van standaard-schepen en duwstellen. De Conferentie van Europese Ministers van Verkeer (CEMT) kwam in 1992 met een klassering voor bevaarbaarheid. Aan de klassen I tot en met VII werd voor Nederland een klasse 0 toegevoegd, voor vaarwegen kleiner dan klasse I. Klasse VII is niet overgenomen: duwvaart met negen bakken komt niet voor in Nederland. De klassen zijn gebaseerd op de volgende zaken: - lengte en de breedte van het vaartuig (het grootste duwstel). - breedte en diepte van de vaarweg. - kunstwerken (schutlengte, wijdte, drempeldiepte en hoogte van hefdeuren en/of vaste bruggen). - eventuele (scherpe) bochten in de vaarweg. De volgende klassen worden onderkend: 0 Kleine vaartuigen en recreatievaart I Spits II Kempenaar III Dortmund - Eemskanaalschip IV Rijn - Hernekanaalschip, Eenbaksduwstel Va Groot Rijnschip, Eenbaksduwstel Vb Tweebaksduwstel (lange formatie) VIa Tweebaksduwstel (brede formatie) VIb Vierbaksduwstel VIc Zesbaksduwstel 

## API adres 
https://data.pdok.nl/http-geodata-nationaalgeoregister-nl-vin-wfs-

## Service adressen: 
Type: OGC:WFS [http://geodata.nationaalgeoregister.nl/vin/wfs?](http://geodata.nationaalgeoregister.nl/vin/wfs?) 

Type: OGC:WMS [http://geodata.nationaalgeoregister.nl/vin/wms?](http://geodata.nationaalgeoregister.nl/vin/wms?) 

Type: INSPIRE Atom [http://geodata.nationaalgeoregister.nl/vin/atom/vin.xml](http://geodata.nationaalgeoregister.nl/vin/atom/vin.xml) 

Type: download [http://www.rijkswaterstaat.nl/apps/geoservices/geodata/dmc/vaarweginformatie_nederland/geogegevens/shape](http://www.rijkswaterstaat.nl/apps/geoservices/geodata/dmc/vaarweginformatie_nederland/geogegevens/shape) 

Type: download [http://www.rijkswaterstaat.nl/apps/geoservices/geodata/dmc/vaarweginformatie_nederland/productinfo/beschrijvende_documentatie/](http://www.rijkswaterstaat.nl/apps/geoservices/geodata/dmc/vaarweginformatie_nederland/productinfo/beschrijvende_documentatie/) 

