---
layout: dataset
title: Stiltegebieden
date: Mon Nov 16 2015 16:27:08 GMT+0100 (CET)
modified: 2011-03-09
tags: ffffffff-7bc1-50bf-8c40-a6e7c562935a, Stiltegebieden, 
---
## Toelichting: 
Het begrip stiltegebied vindt zijn oorsprong in de Wet geluidhinder (Staatsblad, 1979). Stiltegebieden waren daarin gedefinieerd als gebieden waarin de geluidbelasting door toedoen van menselijke activiteiten zo laag is, dat de in dat gebied heersende natuurlijke geluiden niet of nauwelijks worden gestoord. In 1993 is het artikel over stiltegebieden in de Wet geluidhinder komen te vervallen en het wettelijke kader overgenomen in de Wetmilieubeheer.De stiltegebieden maken deel uit van de Provinciale Omgevingsverordening Drenthe, van 14 april 2011. 

## Service adressen: 
Type: OGC:WMS [http://www.drenthe.info/geoserver/zoek/wms](http://www.drenthe.info/geoserver/zoek/wms) 

Type: OGC:WFS [http://www.drenthe.info/geoserver/zoek/wfs](http://www.drenthe.info/geoserver/zoek/wfs) 

