#!/usr/bin/env bash
cd /opt/queuemanager
#npm install
forever -a -l /opt/logs/queuemanager.log -o /opt/logs/queuemanager.out.log -e /opt/logs/queuemanager.error.log /opt/queuemanager/app.js
