var _ = require('lodash'),
  app = require('express')(),
  request = require('request'),
  winston = require('winston'),
  Promise = require('bluebird');

var logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({ timestamp: true })
    ]
});

var baseUrl = 'http://inu346.in.kadaster.nl:3030';

app.get('/:endpoint/', function(req, res) {
  getApi(req.params.endpoint)
    .then(function(api) {
      logger.info('Started processing endpoint "%s".', req.params.endpoint);
      res.status(202).send({ message: 'Done' });
      return _.map(api._links, function(link) {
        return baseUrl + link.href.replace('/apis/', '/');
      });
    })
    .mapSeries(function(endpoint) {
      logger.info('Importing collection "%s".', endpoint);
      return importCollection(endpoint, 'http://data.pdok.nl/' + req.params.endpoint, 1);
    })
    .then(function() {
      logger.info('Finished processing endpoint "%s".', req.params.endpoint);
    })
    .catch(function(err) {
      logger.error(err.message);
      res.status(500).send({ message: err.message });
    });
});

function getApi(endpoint) {
    console.log('getApi says: got ' + baseUrl + '/' + endpoint + '/v1.0/')
  return new Promise(function(resolve, reject) {
    request({
        method: 'GET',
        proxy: null,
        url: baseUrl + '/' + endpoint + '/v1.0/',
        json: true
    }, function(err, response, body) {
      if (err) {
        return reject(err);
      }

      if (response.statusCode !== 200) {
        return reject(new Error('Error retrieving API data.'));
      }

      resolve(body);
    });
  });
}

function importCollection(endpoint, graphUri, page) {
  return new Promise(function(resolve, reject) {
    request({
      method: 'GET',
      proxy: null,
      url: endpoint + '?limit=100&page=' + page,
      headers: {
        'Accept': 'application/n-triples'
      },
    }, function(err, response, body) {
      if (err) {
        return reject(err);
      }

      if (response.statusCode !== 200) {
        return resolve();
      }

      sendTriples(graphUri, body)
        .then(function() {
          return importCollection(endpoint, graphUri, page + 1);
        })
        .then(function() {
          resolve();
        })
        .catch(function(err) {
          reject(err);
        });
    });
  });
}

function sendTriples(graphUri, triples) {
  return new Promise(function(resolve, reject) {
    request({
      method: 'POST',
      proxy: null,
      url: 'http://inu346.in.kadaster.nl:8890/sparql-graph-crud-auth?graph-uri=' + encodeURIComponent(graphUri),
      body: triples,
      auth: {
        user: 'dba',
        pass: 'dba',
        sendImmediately: false
      }
    }, function(err, response, body) {
      if (err) {
        return reject(err);
      }

      resolve();
    });
  });
}

var server = app.listen(3010, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Harvester app listening at http://%s:%s', host, port);
});
