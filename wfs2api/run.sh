#!/usr/bin/env bash
cd /opt/api && npm install
forever -a -w --watchDirectory /opt/wfs2api/swagger -l /opt/logs/api.log -o /opt/logs/api.out.log -e /opt/logs/api.error.log /opt/wfs2api/app.js
