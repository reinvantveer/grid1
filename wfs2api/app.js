var express = require('express');
var app = express();
var SwaggerParser = require('swagger-parser');
var util = require('util');
var request = require('request');
var JSONLD = require('jsonld');
var fs = require('fs');
var negotiate = require('express-negotiate');
var wkt = require('wellknown');
var json2csv = require('json2csv');
var crypto = require('crypto');

// globals
var contexts = [];
var apis = [];
var defaultLimit = 20;
var wfsDefaults = '?service=WFS&version=2.0.0&outputFormat=json&srsName=EPSG:4326&request=';
var reservedQueryParams = ['id', 'page', 'limit'];

app.use(
  function(req, res, next) {
    res.set('Acces-Control-Allow-Origin', '*');
    next();
  }
)

// root
app.get('/', function (req, res) {

  var response = {
    _links: {}
  };

  for (api in apis) {
    response._links[api] = {
      'href': '/apis/' + api + '/v1.0/'
    };
  }

  if (req.header('Accept').indexOf('text/html') > -1) {
    res.send('<pre>' + JSON.stringify(response, undefined, 2) + '</pre>');
  } else {
    res.send(response);
  }
});

// versions
app.get('/:name/', function (req, res) {
  if (!apis[req.params.name]) {
    res.status(404).send('Not found');
  }

  var response = {
    _links: {
      'v1.0' : {
        'href': '/apis/' + req.params.name + '/v1.0/'
      }
    }
  };

  if (req.header('Accept').indexOf('text/html') > -1) {
    res.send('<pre>' + JSON.stringify(response, undefined, 2) + '</pre>');
  } else {
    res.send(response);
  }
});

// index
app.get('/:name/:version/', function (req, res) {
  if (!apis[req.params.name]) {
    res.status(404).send('Not found');
  }
console.log('Collections requested.');
  SwaggerParser.bundle(__dirname + '/swagger/' + req.params.name + '.json').then(function(swagger) {

    var response = {
      _links: {}
    };

    for (definition in swagger.definitions) {
      response._links[definition] = {
        'href': '/apis/' + req.params.name + '/' + req.params.version + '/' + definition.toLowerCase() + '/'
      };
    }

/*
    if (req.header('Accept').indexOf('text/html') > -1) {
      res.send('<pre>' + JSON.stringify(response, undefined, 2) + '</pre>');
    } else {
*/
      res.send(response);
//    }
  });
});

// context
app.get('/:name/:version/context.jsonld', function (req, res) {
  if (!contexts[req.params.name]) {
    res.status(404).send('Not found');
  }

  res.send({'@context' : contexts[req.params.name]['@context']});
});

// collection
app.get('/:name/:version/:collection/', function (req, res) {
  if (!apis[req.params.name]) {
    res.status(404).send('Not found');
  }
console.log('Collection ' + req.params.collection + ' requested.');
  page = req.query.page ? Number(req.query.page) : 1;
  limit = req.query.limit ? req.query.limit : defaultLimit;
  offset = (limit * page) - limit;

  SwaggerParser.bundle(__dirname + '/swagger/' + req.params.name + '.json').then(function(swagger) {
    doRequest(swagger, 'GetFeature&count=' + limit + '&startindex=' + offset + '&typename=' + req.params.collection, req, res);
  });
});

// resource
app.get('/:name/:version/:collection/:resource/', function (req, res) {
  if (!apis[req.params.name]) {
    res.status(404).send('Not found');
  }

  SwaggerParser.bundle(__dirname + '/swagger/' + req.params.name + '.json').then(function(swagger) {
    doRequest(swagger, 'GetFeature&typename=' + req.params.collection + '&featureid=' + req.params.resource, req, res);
  });
});

function doRequest(swagger, requestUri, req, res) {
  var defName = req.params.collection.charAt(0).toUpperCase() + req.params.collection.slice(1);
  var model = swagger.definitions[defName];

  allowedFilters = [];
  for (allowedFilter in model.properties) {
    if (model.properties[allowedFilter].type != 'object') {
      allowedFilters.push(allowedFilter);
    }
  }

  cqlFilters = [];
  for (filter in req.query) {
    if (reservedQueryParams.indexOf(filter) == -1 && allowedFilters.indexOf(filter) > 0){
      cqlFilters.push(filter + '=%27' + require('querystring').escape(req.query[filter]) + '%27');
    }
  }

  if (cqlFilters.length) {
    requestUri = requestUri + '&cql_filter=(' + cqlFilters.join(' AND ') + ')';
  }

  console.log(swagger.info['x-wfs-endpoint'] + wfsDefaults + requestUri);

  request({
      method: 'GET',
      tunnel: false,
      proxy: 'http://ssl-proxy.kadaster.nl:8080',
      timeout: 120000,
      url: swagger.info['x-wfs-endpoint'] + wfsDefaults + requestUri
  }, function(error, response, body) {
    if (error) {
        console.error(err.stack);
      res.status(408).send('Request Timeout');
      return;
    }

    if (response.statusCode > 200) {
      res.send('Wrong statuscode: ' + response.statusCode);
      return;
    }

    if (body[0] == '<') {
      res.set('Content-Type', 'text/html');
      res.send('Unknown error. WFS reports: <pre>' + body + '</pre>');
      return;
    }

    var body = JSON.parse(body);
    if (!body.features.length) {
      res.status(404).send('No results');
      return;
    }

    var results = [];
    var geometryName = '';

    body.features.forEach(function(feature){
      result = {};
      for (property in model.properties) {
        if (property == 'id') {
          result.id = feature.id;
        } else if(property == feature.geometry_name) {
          geometryName = feature.geometry_name;
          result[feature.geometry_name] = {};
          result[feature.geometry_name]['geoJson'] = {
            'type': feature.geometry.type,
            'coordinates': feature.geometry.coordinates,
            'crs': {
              'type': 'name',
              'properties': {
                'name': 'urn:ogc:def:crs:OGC:1.3:CRS84'
              }
            }
          };
          if (req.get('Accept').indexOf('application/n-triples') > -1 || req.get('Accept').indexOf('application/nquads') > -1) {
            var wktString = wkt.stringify(result[feature.geometry_name]['geoJson']);
            result[feature.geometry_name]['wkt'] = wktString;
            result[feature.geometry_name]['@id'] = '/geometry/' + crypto.createHash('sha1').update(wktString).digest('hex');
          }
        } else {
          result[property] = feature.properties[property] ? feature.properties[property] : null;
        }
      }

      if (!req.params.resource) {
        result._links = {
          'self': {
            'href': '/apis/' + req.params.name + '/' + req.params.version + '/' + req.params.collection + '/' + feature.id + '/'
          }
        }
      }

      results.push(result);
    });

    page = req.query.page ? Number(req.query.page) : 1;

    var newResponse = {
      '_embedded': {
        'results': results
      },
      '_links': {
        'self': {
          'href': '/apis/' + req.params.name + '/' + req.params.version + '/' + req.params.collection + '/' + (page > 1 ? '?page=' + page : '')
        }
      }
    };

    limit = req.query.limit ? req.query.limit : defaultLimit;

    if (page * limit < body.totalFeatures) {
      newResponse._links.next =  {
        'href': '/apis/' + req.params.name + '/' + req.params.version + '/' + req.params.collection + '/?page=' + (page + 1)
      };
    }

    response = req.params.resource ? result : newResponse;

    if (req.header('Accept') == 'application/ld+json' || req.header('Accept') == 'application/nquads' || req.header('Accept') == 'application/n-triples') {
      if (contexts[req.params.name]) {
        var context = { '@context' : contexts[req.params.name]['@context'] };
      } else {
        var context = {
          '@context' : {
            '@base': 'http://data.pdok.nl/' + req.params.name + '/',
            'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
            'ogcgs': 'http://www.opengis.net/ont/geosparql#',
            'naam': 'rdfs:label',
            'buurtnaam': 'rdfs:label',
            'pv_naam': 'rdfs:label',
            'gm_naam': 'rdfs:label',
            'kern_naam': 'rdfs:label',
            'vwg_naam': 'rdfs:label',
            'NAAM': 'rdfs:label',
            'TOPONIEM': 'rdfs:label',
            'tekst': 'rdfs:label',
            'jaar': 'http://data.pdok.nl/year',
            'datum': 'http://data.pdok.nl/date',
            'vwk_begdtm': 'http://data.pdok.nl/date',
            'status': 'http://data.pdok.nl/status',
            'shape_len': 'http://data.pdok.nl/shapeLength',
            'shape_leng': 'http://data.pdok.nl/shapeLength',
            'shape_area': 'http://data.pdok.nl/shapeArea',
            'afstand': 'http://data.pdok.nl/distance',
            '_embedded': '@graph',
            'results': '@graph',
            'wkt' : {
              '@id' : 'ogcgs:asWKT',
              '@type' : 'ogcgs:wktLiteral'
            }
          }
        };
        context['@context'][geometryName] = 'ogcgs:hasGeometry';
      }

      if (!req.params.resource) {
        response._embedded['@id'] = context['@context']['@base'];
      }

      if (contexts[req.params.name] && contexts[req.params.name]['@x-context'][req.params.collection]) {
        var idUri = contexts[req.params.name]['@x-context'][req.params.collection]['@id'];
        var typeUri = contexts[req.params.name]['@x-context'][req.params.collection]['@type'];
      } else{
        var idUri = 'http://data.pdok.nl/' + req.params.name + '/' + req.params.collection + '/{id}/';
        var typeUri = 'http://data.pdok.nl/' + req.params.name + '/' + req.params.collection.charAt(0).toUpperCase() + req.params.collection.slice(1) + '/';
      }

      if (!req.params.resource) {
        for (result in response._embedded.results) {
          response._embedded.results[result]['@id'] = idUri.replace(/\{(\w+)\}/g, function(match, key) {
            return response._embedded.results[result][key];
          });
          response._embedded.results[result]['@type'] = typeUri;
        }
      } else {
        response['@id'] = idUri.replace(/\{(\w+)\}/g, function(match, key) {
          return response[key];
        });
        response['@type'] = typeUri;
      }
    }

    req.negotiate({
      'application/json': function() {
        if (contexts[req.params.name]) {
          res.set('Link', '</apis/' + req.params.name + '/' + req.params.version + '/context.jsonld>; rel="http://www.w3.org/ns/json-ld#context"; type="application/ld+json"');
        }

        res.set('Content-Type', 'application/json');
        res.json(response);
      },
      'application/nquads': function() {
        res.set('Content-Type', 'application/nquads');
        JSONLD.toRDF(response, { format: 'application/nquads', expandContext: context }, function(err, nquads) {
          res.send(nquads);
        });
      },
      'application/n-triples': function() {
        res.set('Content-Type', 'application/n-triples');
        JSONLD.toRDF(response, { format: 'application/nquads', expandContext: context }, function(err, ntriples) {
          res.send(ntriples.replace(new RegExp('<' + context['@context']['@base'] + '> .', 'g'), '.'));
        });
      },
      'application/ld+json': function() {
        res.set('Content-Type', 'application/ld+json');
        response['@context'] = context['@context'];
        res.json(response);
      },
      'text/html': function() {
        res.set('Content-Type', 'text/html');
        res.send('<pre>' + JSON.stringify(response, undefined, 2) + '</pre>')
      },
      'text/csv': function() {
        res.set('Content-Type', 'text/csv');
        if (response._embedded) {
          response = response._embedded.results;
          for (result in response) {
            response[result][geometryName] = response[result][geometryName].wkt;
            response[result]._links = null;
            delete response[result]._links;
          }
        } else {
          response[geometryName] = response[geometryName].wkt;
          response._links = null;
          delete response._links;
        }

        // no column headers when paging through higher pages
        json2csv({ data: response, hasCSVColumnTitle: (page <= 1) }, function(err, csv) {
          if (err) console.error(err);
          res.send(csv);
        });
      }
    });
  });
}

var server = app.listen(3030, function () {
  var host = server.address().address;
  var port = server.address().port;

  fs.readdirSync(__dirname + '/context').forEach(function(file){
    contexts[file.replace('.jsonld', '')] = JSON.parse(fs.readFileSync(__dirname + '/context/' + file));
  });

  fs.readdirSync(__dirname + '/swagger').forEach(function(file){
      apis[file.replace('.json', '')] = file;
  });

  console.log('Example app listening at http://%s:%s', host, port);
});
