# PDOK proeftuin APIficatie en Linked Data

In deze repository staan scripts voor de opbouw van een proeftuin voor nieuwe manieren van ontsluiting voor de services van PDOK. Deze bieden meerwaarde op verschillende fronten: meer generieke ontsluiting voor webdevelopers in brede zin (API's) en verrijkte ontsluiting voor de Semantisch Web wereld (Linked Data, SPARQL)

## Requirements
* Docker
* Docker compose

## Uitvoeren

    docker-compose --project-name grid1 up -d

## Herstarten
  
    docker-compose restart

See also docker-compose [reference](https://docs.docker.com/compose/reference/docker-compose/)