'use strict';

var _ = require('lodash'),
  xpath = require('xpath'),
  request = require('request'),
  DOMParser = require('xmldom').DOMParser,
  Promise = require('bluebird');

var select = xpath.useNamespaces({
  ows: 'http://www.opengis.net/ows/1.1',
  xsd: 'http://www.w3.org/2001/XMLSchema'
});

module.exports = function(options) {
  var spec = {
    swagger: '2.0',
    info: {
      version: options.version,
      'x-wfs-endpoint': options.wfs,
    },
    host: options.host,
    basePath: options.basePath,
    schemes: ['http'],
    produces: ['application/json', 'application/nquads', 'application/n-triples', 'application/ld+json'],
    paths: {},
    definitions: {}
  };

  return getCapabilities(options.wfs)
    .then(function(capabilities) {
      spec.info.title = select('//ows:ServiceIdentification/ows:Title/text()', capabilities).toString();
      spec.info.description = select('//ows:ServiceIdentification/ows:Abstract/text()', capabilities).toString();
      spec.info.contact = {
        name: select('//ows:ServiceProvider/ows:ProviderName/text()', capabilities).toString()
      };

      return getFeatureTypes(options.wfs);
    })
    .map(function(featureType) {
      var name = featureType.getAttribute('name').replace(/Type$/, '');
      var defName = name.charAt(0).toUpperCase() + name.slice(1);

      return getExampleData(options.wfs, name)
        .then(function(exampleData) {
          spec.paths['/' + name + '/'] = createCollectionEndpoint(featureType, defName, options.basePath + '/' + name + '/');
          spec.paths['/' + name + '/{id}/'] = createResourceEndpoint(featureType, defName, options.basePath + '/{id}/');
          spec.definitions[defName] = createDefinition(featureType, defName, exampleData);
        });
    })
    .then(function() {
      return spec;
    });
}

function getCapabilities(url) {
  return new Promise(function(resolve, reject) {
    request.get({
      url: url,
      proxy: 'http://ssl-proxy.kadaster.nl:8080',
      tunnel: false,
      qs: {
        request: 'GetCapabilities'
      }
    }, function(err, response, body) {
      var xmlDoc, featureTypes;
      if (err) return reject(err);

      try {
        xmlDoc = new DOMParser().parseFromString(body);
      } catch (err) {
        return reject(err);
      }

      resolve(xmlDoc);
    });
  });
}

function getFeatureTypes(url) {
  return new Promise(function(resolve, reject) {
    request.get({
      url: url,
      proxy: 'http://ssl-proxy.kadaster.nl:8080',
      tunnel: false,
      qs: {
        request: 'DescribeFeatureType'
      }
    }, function(err, response, body) {
      var xmlDoc;
      if (err) return reject(err);

      try {
        xmlDoc = new DOMParser().parseFromString(body);
      } catch (err) {
        return reject(err);
      }

      resolve(select('/xsd:schema/xsd:complexType', xmlDoc));
    });
  });
};

function getExampleData(wfs, name) {
  return new Promise(function(resolve, reject) {
    request.get({
      url: wfs,
      proxy: 'http://ssl-proxy.kadaster.nl:8080',
      tunnel: false,
      json: true,
      qs: {
        service: 'WFS',
        version: '2.0.0',
        request: 'GetFeature',
        typeName: name,
        outputFormat: 'json',
        startIndex: 0,
        count: 10,
        srsName: 'EPSG:4326'
      }
    }, function(err, response, body) {
      var exampleData = {};
      if (err) return reject(err);

      _.forEach(body.features, function(feature) {
        exampleData.id = feature.id;
        _.forEach(feature.properties, function(property, key) {
          if (property !== null) {
            exampleData[key] = property;
          }
        });
      });

      resolve(exampleData);
    });
  });
}

function createDefinition(featureType, name, exampleData) {
  var required = [];
  var elements = select('xsd:complexContent/xsd:extension/xsd:sequence/xsd:element', featureType);

  var properties = {
    id: {
      type: 'string',
      example: exampleData.id
    }
  };

  var def = {
    type: 'object',
    properties: _.reduce(elements, function(result, element) {
      var name = element.getAttribute('name');
      result[name] = convertSchema(element, exampleData[name]);
      if (element.getAttribute('nillable') !== 'true') {
        required.push(name);
      }
      return result;
    }, properties)
  };

  if (required.length > 0) {
    def.required = required;
  }

  return def;
}

function createCollectionEndpoint(featureType, name, path) {
  var parameters = [
    {
      name: 'page',
      in: 'query',
      type: 'integer',
      required: false,
      description: 'Paginanummer voor paginering.'
    }
  ];

  var elements = select('xsd:complexContent/xsd:extension/xsd:sequence/xsd:element', featureType);

  _.forEach(elements, function(element) {
    if (!element.getAttribute('type').match(/^gml:/)) {
      parameters.push(convertParameter(element));
    }
  });

  return {
    get: {
      summary: name + ' collection call (overzicht). Gepagineerd per 20 records.',
      parameters: parameters,
      responses: {
        '200': {
          description: 'OK',
          schema: {
            type: 'object',
            required: ['_embedded', '_links'],
            properties: {
              _embedded: {
                type: 'object',
                required: ['results'],
                properties: {
                  results: {
                    type: 'array',
                    items: {
                      $ref: '#/definitions/' + name
                    }
                  }
                }
              },
              _links: {
                type: 'object',
                required: ['self'],
                properties: {
                  self: {
                    type: 'object',
                    required: ['href'],
                    properties: {
                      href: {
                        type: 'string',
                        example: path,
                        description: 'Link naar huidige pagina'
                      }
                    }
                  },
                  next: {
                    type: 'object',
                    required: ['href'],
                    properties: {
                      href: {
                        type: 'string',
                        example: path + '?page=2',
                        description: 'Link naar volgende pagina'
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  };
}

function createResourceEndpoint(featureType, name, path) {
  return {
    get: {
      summary: name + ' resource call (detail).',
      parameters: [
          {
            name: 'id',
            in: 'path',
            type: 'string',
            required: true,
            description: 'Identifier'
          }
      ],
      responses: {
        '200': {
          description: 'OK',
          schema: {
            $ref: '#/definitions/' + name
          }
        },
        '404': {
          description: 'Not found',
        }
      }
    }
  };
}

function convertSchema(element, example) {
  var type = element.getAttribute('type');

  if (type.match(/^gml:/)) {
    return {
      type: 'object',
      properties: {
        type: {
          type: 'string',
          example: 'Polygon'
        },
        coordinates: {
          type: 'array',
          items: {
            type: 'number'
          },
          example: [ 6.861053257304809, 53.32271304611565 ]
        },
        crs: {
          type: 'object',
          properties: {
            type: {
              type: 'string',
              example: 'name'
            },
            properties: {
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'urn:ogc:def:crs:OGC:1.3:CRS84'
                }
              }
            }
          }
        }
      }
    };
  }

  switch (type) {
    case 'xsd:decimal':
    case 'xsd:double':
    case 'xsd:long':
      return {
        type: 'integer',
        example: _.isUndefined(example) ? 1 : example
      };
    default:
      return {
        type: 'string',
        example: _.isUndefined(example) ? 'String' : example
      };
  }
}

function convertParameter(element) {
  var parameter = {
    name: element.getAttribute('name'),
    in: 'query',
    required: false,
    description: 'Filteren op ' + element.getAttribute('name')
  };

  switch (element.getAttribute('type')) {
    case 'xsd:decimal':
    case 'xsd:double':
    case 'xsd:long':
      parameter.type = 'integer';
      break;
    default:
      parameter.type = 'string';
  }

  return parameter;
}
