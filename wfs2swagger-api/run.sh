#!/usr/bin/env bash
npm config set proxy http://ssl-proxy.kadaster.nl:8080
npm config set https-proxy https://ssl-proxy.kadaster.nl:8080
npm config set strict-ssl false
cd /opt/wfs2swagger-api
# npm install
forever -a -l /opt/logs/wfs2swagger-api.log -o /opt/logs/wfs2swagger-api.out.log -e /opt/logs/wfs2swagger-api.error.log /opt/wfs2swagger-api/app.js
