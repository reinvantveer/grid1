var express = require('express');
var app = express();
var wfs2swagger = require('./lib/wfs2swagger');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
  extended: true
}));

app.post('/', createSwaggerSpec);

function createSwaggerSpec(req, res) {
    console.log('Creating Swagger specification')
  wfs2swagger(req.body).then(function(spec){
    res.status(200).send(spec)
  }).catch(function(err){
    console.error(err);
    res.status(500).send({
      message: 'Unknown error'
    });
  })
}

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('wfs2swagger api app listening at http://%s:%s', host, port);
});
