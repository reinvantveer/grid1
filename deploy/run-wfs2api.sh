#!/usr/bin/env bash
sudo docker run -d -it -P --restart=always \
    -p 3030:3030 \
    --volume $PWD/../wfs2api:/opt/wfs2api \
    --volume $PWD/../logs:/opt/logs \
    --name wfs2api \
    pdok/grid1-wfs2api
