#!/usr/bin/env bash
sudo docker build -t pdok/grid1-apache ./apache && \
sudo docker build -t pdok/grid1-jekyll ./jekyll && \
sudo docker build -t pdok/grid1-linkeddatatheatre ./linkeddatatheatre && \
sudo docker build -t pdok/grid1-metadata-api ./metadata-api && \
sudo docker build -t pdok/grid1-wfs2swagger-api ./wfs2swagger-api && \
sudo docker build -t pdok/grid1-virtuoso ./virtuoso && \
sudo docker build -t pdok/grid1-queuemanager ./queuemanager && \
sudo docker build -t pdok/grid1-wfs2api ./wfs2api && \
echo 'Containers built'
