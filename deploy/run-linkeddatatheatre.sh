#!/usr/bin/env bash
sudo docker run -d -it -P --restart=always \
    -p 8080:8080 \
    --volume $PWD/../../linkeddatatheatre/ROOT:/tomcat/webapps/ROOT \
    --volume $PWD/../../linkeddatatheatre:/opt/linkeddatatheatre \
    --volume $PWD/../logs:/logs \
    --link virtuoso:virtuoso \
    --name linkeddatatheatre \
    pdok/grid1-linkeddatatheatre
