#!/usr/bin/env bash
# Build and run wfs2swagger container
sudo docker run -d -it -P --restart=always \
    -p 3000:3000 \
    --volume $PWD/../wfs2swagger-api:/opt/wfs2swagger-api \
    --volume $PWD/../logs:/opt/logs \
    --name wfs2swagger \
    pdok/grid1-wfs2swagger
