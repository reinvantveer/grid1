#!/usr/bin/env bash
sudo docker run -d -it --restart=always -p 80:80 \
    --volume $PWD/../apache/sites-available:/etc/apache2/sites-available \
    --volume $PWD/../apache/var/www:/var/www  \
    --volume $PWD/../apache/etc/security:/etc/security \
    --volume $PWD/../logs/apache2:/var/log/apache2 \
    --link jekyll:jekyll \
    --link wfs2swagger:wfs2swagger \
    --link virtuoso:virtuoso \
    --link linkeddatatheatre:linkeddatatheatre \
    --link wfs2api:wfs2api \
    --link metadataapi:metadataapi \
    --name apache \
    pdok/grid1-apache
