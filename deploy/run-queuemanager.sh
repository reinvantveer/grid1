#!/usr/bin/env bash
sudo docker run -d -it -P --restart=always \
    -p 3010:3010 \
    --volume $PWD/../queuemanager:/opt/queuemanager \
    --volume $PWD/../logs:/opt/logs \
    --link virtuoso:virtuoso \
    --link wfs2api:wfs2api \
    --name queuemanager \
    pdok/grid1-queuemanager
