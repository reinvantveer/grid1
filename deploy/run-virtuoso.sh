#!/usr/bin/env bash
sudo docker run -d -it -P --restart=always \
    -p 8890:8890 \
    --volume $PWD/../virtuoso/virtuoso-persist/virtuoso/db:/var/lib/virtuoso/db \
    --volume $PWD/../virtuoso/virtuoso-persist/data:/import_store \
    --volume $PWD/../virtuoso/virtuoso-persist/vad:/usr/share/virtuoso/vad \
    --name virtuoso \
    pdok/grid1-virtuoso
