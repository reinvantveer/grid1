#!/usr/bin/env bash
sudo docker rm -f linkeddatatheatre
sudo docker run -it -P --restart=always \
    -p 8080:8080 \
    --volume $PWD/../../linkeddatatheatre/ROOT:/tomcat/webapps/ROOT \
    --volume $PWD/../../linkeddatatheatre:/opt/linkeddatatheatre \
    --link virtuoso:virtuoso \
    --name linkeddatatheatre \
    pdok/lod-linkeddatatheatre

./rerun-apache.sh