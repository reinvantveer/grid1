#!/usr/bin/env bash
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
sudo docker ps -a | grep weeks ago | awk {print } | xargs --no-run-if-empty sudo docker rm
