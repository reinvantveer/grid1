#!/usr/bin/env bash
./run-jekyll.sh
./run-wfs2swagger.sh
./run-metadataapi.sh
./run-virtuoso.sh
./run-linkeddatatheatre.sh
./run-wfs2api.sh
./run-queuemanager.sh
#./run-contexteditor.sh
./run-apache.sh

echo 'Containers running'
