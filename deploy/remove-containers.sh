#!/usr/bin/env bash
sudo docker rm -f apache
sudo docker rm -f jekyll
sudo docker rm -f linkeddatatheatre
sudo docker rm -f virtuoso
sudo docker rm -f metadataapi
sudo docker rm -f wfs2swagger
sudo docker rm -f api2triplestorepopulator
sudo docker rm -f wfs2api
sudo docker rm -f queuemanager
# sudo docker rm -f contexteditor
