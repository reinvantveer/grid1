#!/usr/bin/env bash
echo
echo 'after the image has built and is started, press Control-p, followed by Control-q to detach from the running container'
echo
# docker run -p 3030:3030 -it --restart=always --name jena -e ADMIN_PASSWORD=pdok-lod stain/jena-fuseki

./remove-containers.sh
./build-images.sh
echo 'After the image has been started, press Control-p, followed by Control-q to detach from the running container'
./run-containers.sh
# Rerun apache router container for good measure to ensure routes are properly forwarded
./rerun-apache.sh

# List running containers
echo 'You will now see the output of the command'
echo 'docker ps'
echo 'to verify that the PDOK data platform containers have been started'
sudo docker ps
