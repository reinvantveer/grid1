#!/usr/bin/env bash
# Build and run jekyll container
sudo docker run -d -it -P  --restart=always \
    --volume $PWD/../jekyll:/opt/jekyll \
    --volume $PWD/../logs:/opt/logs \
    --name jekyll \
    pdok/grid1-jekyll
