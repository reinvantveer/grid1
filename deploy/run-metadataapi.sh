#!/usr/bin/env bash
sudo docker run -d -it -P --restart=always \
    -p 3020:3020 \
    --volume $PWD/../queuemanager:/opt/queuemanager \
    --volume $PWD/../wfs2api/swagger:/opt/wfs2api/swagger \
    --volume $PWD/../metadata-api:/opt/metadata-api \
    --volume $PWD/../mongodb-persist:/data/db \
    --volume $PWD/../logs:/opt/logs \
    --link wfs2swagger:wfs2swagger \
    --name metadataapi \
    pdok/grid1-metadataapi
