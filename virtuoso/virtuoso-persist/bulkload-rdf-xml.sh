#!/bin/bash
if (( $# != 1 ))
then
  echo "Usage: bulkload-rdf-xml.sh [your-graph-uri]"
  exit 1
fi
isql 1111 dba dba VERBOSE=OFF "EXEC=\
	ld_dir_all('/import_store', '*.rdf', 'XML/RDF', '$1'); \
	rdf_loader_run();\
	select * from load_list;"