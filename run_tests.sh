#!/usr/bin/env bash
  # Tail logs until 'Sauce Connect is up'
  docker logs -f sauceconnect | while read LINE
  do
    echo "$LINE"
    if [[ "$LINE" == *"Sauce Connect is up"* ]]; then
      pkill -P $$ docker
    fi
  done