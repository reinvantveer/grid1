#!/usr/bin/env bash
echo 'running startup script'
npm config set proxy http://ssl-proxy.kadaster.nl:8080
npm config set https-proxy https://ssl-proxy.kadaster.nl:8080
npm config set strict-ssl false
cd /opt/metadata-api
mongod --fork --logpath /opt/logs/mongodb.log
# Dreaded proxy workaround
mkdir /usr/share/unicode
wget http://unicode.org:80/Public/UNIDATA/UnicodeData.txt -P /usr/share/unicode
npm install
forever -a -l /opt/logs/metadata-api.log -o /opt/logs/metadata-api.out.log -e /opt/logs/metadata-api.error.log /opt/metadata-api/index.js
