'use strict';
var mongoose = require('mongoose');
var sluggable = require('mongoose-sluggable');
var request = require('request');
var xpath = require('xpath');
var dom = require('xmldom').DOMParser;

var DatasetSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    index: true,
    unique: true,
    trim: true
  },
  tags: String,
  description: String,
  wfsURL: {
    type: String,
    required: true
  },
  graphURI: String
}, {
  timestamps: true,
  versionKey: false
});

DatasetSchema.plugin(sluggable, {
  updatable: false,
  source: 'title',
  unique: true
});

function isValidWFS(wfsEndpoint, callback) {

    if (wfsEndpoint.search('http') == -1){
        console.error(wfsEndpoint + ' is no http address');
        return callback(false);
    }

    request(wfsEndpoint, handleWFSResponse(wfsEndpoint, callback));
}

function handleWFSResponse(wfsEndpoint, callback){
    return function (err, response, xmlbody){
        if (err) {
            console.error("Error getting " + wfsEndpoint + ": " + err.stack);
            return callback(false);
        } else if (response.statusCode !== 200) {
            console.error('An error occurred getting ' + wfsEndpoint + ", error: wrong response status code")
            console.error('Statuscode: ' + response.statusCode);
            return callback(false);
        } else if (response.headers['content-type'] !== 'application/xml' && response.headers['content-type'] !== 'text/xml') {
            console.error('An error occurred getting ' + wfsEndpoint + ", error: wrong content type " + response.headers['content-type']);
            return callback(false);
        } else {
            var xmldoc = new dom().parseFromString(xmlbody);

            if (getWFSVersion(xmldoc) !== '2.0.0') {
                console.error("Wrong WFS version: " + getWFSVersion(xmldoc));
                return callback(false);
            } else if (hasExceptionReport(xmldoc)) {
                return callback(false);
            } else if (isCapableOfGeoJSON(xmldoc) === false) {
                console.error("Error: endpoint is not capable of GeoJSON output.");
                return callback(false);
            } else if (hasExceptionReport(xmldoc)) {
                return callback(false);
            } else {
                return callback(true);
            }

        }
    }
}

function getWFSVersion(xmldoc) {
    var owsNamespace = xpath.select('/*/namespace::*[name()="ows"]', xmldoc)[0].namespaceURI;
    var select = xpath.useNamespaces({
        ows: owsNamespace
    });

    return select('//ows:ServiceTypeVersion/text()', xmldoc).toString();
}

function isCapableOfGeoJSON(xmldoc){
  var owsNamespace = xpath.select('/*/namespace::*[name()="ows"]', xmldoc)[0].namespaceURI;
  var select = xpath.useNamespaces({
    ows: owsNamespace
  });

  var json = select("//ows:OperationsMetadata/ows:Operation[@name='GetFeature']/ows:Parameter[@name='outputFormat']/ows:AllowedValues/ows:Value[text()='json']/text()", xmldoc).toString()
  return (json === 'json');
}

function hasExceptionReport(xmldoc) {
    var owsNamespace = xpath.select('/*/namespace::*[name()="ows"]', xmldoc)[0].namespaceURI;
    var select = xpath.useNamespaces({
        ows: owsNamespace
    });

    try {
        var report = select('//ows:ExceptionReport', xmldoc).toString();
    } catch (err) {
        return false;
    }

    return report ? true : false;
}

function isCapableOfPagination(wfsEndpoint, typeName, callback) {
    var paginationRequestParams = '?service=WFS&version=2.0.0&outputFormat=json&srsName=EPSG:4326&request=GetFeature&count=10&startindex=1';
    var paginateRequest = wfsEndpoint + paginationRequestParams + '&typename=' + typeName;

    request.get(paginateRequest, handlePaginatedResponse(callback));
}

function handlePaginatedResponse(callback){
    return function verifyException(err, response, body) {
        if (err){
            console.error('isCapableOfPagination threw error: ' + err.stack);
            return callback(false);
        }

        var contentType = response.headers['content-type'];
        if (contentType == "application/xml" || contentType == "text/xml"){
            var xmldoc = new dom().parseFromString(body);
            var hasException = hasExceptionReport(xmldoc);
            return hasException ? callback(false) : callback(true);
        } else {
            return callback(true);
        }
    }
}


module.exports = mongoose.model('Dataset', DatasetSchema);
module.exports.isValidWFS = isValidWFS;
module.exports.getWFSVersion = getWFSVersion;
module.exports.isCapableOfGeoJSON = isCapableOfGeoJSON;
module.exports.isCapableOfPagination = isCapableOfPagination;
module.exports.hasExceptionReport = hasExceptionReport;
