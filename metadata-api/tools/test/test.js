var assert = require('assert');
var tools = require('../index.js');
var fs = require('fs');
var path = require('path');

var configFile = path.join(__dirname + '/../config.json');
var dirtywfsname = 'https://www.myaddress/simpleTitle-wfs-MapServer-WFSServer?request=getCapabilities';
var wfsname = 'https://geodata.nationaalgeoregister.nl/bag/wfs';
var wrongwfsname = 'https://geodata.nationaalgeoregister.nl/balderdash';
var config, configJSON;

beforeEach(function(){
    configJSON = fs.readFileSync(configFile, 'utf-8');
});

describe('metadatatools', function tests() {

    it('should return true, just to see whether Mocha is functioning', function hasMocha() {
        assert.equal(true, true, 'this is true');
    });


    it('should check for the presence of a config.json file', function checkConfigFile() {
        assert.doesNotThrow(function(){
            config = fs.readFileSync(configFile, 'utf-8');
        });
    });

    it('should check if the config file is valid JSON', function validConfigJSON(){
        assert.doesNotThrow(function parseJSON(){
            config = JSON.parse(configJSON);
        });
    });

    it('should recognise undefined config keys', function assertSwaggerDir() {
        assert.equal(config.qwer, undefined);
    });

    it('should verify the presence of the swagger spec dir in the config', function assertSwaggerDir() {
        assert.notEqual(config.wfs2swaggerURL, undefined);
    });

    it('should verify the presence of the swagger spec dir in the config', function assertSwaggerDir() {
        assert.notEqual(config.swaggerSpecDir, undefined);
    });

    it('should verify the presence of the queue manager url in the config', function assertSwaggerDir() {
        assert.notEqual(config.queueManagerURL, undefined);
    });

    it('should verify the presence of the queue manager url in the config', function assertSwaggerDir() {
        assert.notEqual(config.metadataSaveURL, undefined);
    });

    it('should provide a string with only alphanumerical characters and dashes', function testDashify() {
        assert.equal(tools.dashify('abcd\?\(\#\&\%'), 'abcd-');
    });

    it('should return "simpleTitle" for address ' + dirtywfsname, function testTitleify(){
        assert.equal('simpleTitle', tools.titleify(dirtywfsname));
    });

    it('should strip off request parameters and question marks off a URL', function testCleanURL() {
        assert.equal('https://www.myaddress/simpleTitle-wfs-MapServer-WFSServer', tools.cleanWFSendpoint(dirtywfsname));
    });

    var timeout = 30000;
    this.timeout(timeout);

    it('should provide a valid swagger spec for a valid WFS 2.0 endpoint with json output ', function testWFS2Swagger(done) {
        tools.generateSwaggerSpec(wfsname, function (spec) {
            assert.doesNotThrow(function () {
                JSON.parse(spec);
            });
            done();
        });
    });

    this.timeout(60000);
    it('should throw an error for an address that is not a proper WFS endpoint address', function testFaultyWFSaddress(done) {
        var spec = tools.generateSwaggerSpec(wrongwfsname, function (spec) {
            assert.throws(function () {
                JSON.parse(spec);
            });
            done();
        });
    });

});
