'use strict';

var request = require('request'); //for getting metadata, beware of proxy settings!!!
var fs = require('fs'); //for saving metadata files
var path = require('path');

var xml2js = require('xml2js'); //for parsing Nationaal Georegister metadata
var parser = new xml2js.Parser();

var async = require('async');
var ngrtools = require('../index.js');

var swaggerspecdir = '/userdata/docker/grid1/wfs2api/swagger/';
var metadataURL = 'http://www.nationaalgeoregister.nl/geonetwork/srv/dut/q?fast=index&from=1&to=2500&protocol=OGC:WFS&relation=within';
var tempmetadata = {
    datasets: []
};

var wfsEndpoints = [];

/**
 * Generate all jekyll page (meta)data and swagger specs
 * This gets executed as the script is done as an immediately invoked function expression (IIFE)
 *
 * @param metadataURL: URL for the nationaal georegister XML metadata
 */
(function main(metadataURL) {
    console.log('Transformation procedure started');

    request(
        {
            uri: metadataURL,
            proxy: null,
            timeout: 120000
        },
        processRequestResults
    );
})(metadataURL);

/**
 * Process the request results coming from the http request to the nationaal georegister XML URL
 * and writes them to disk as a JSON file
 * @param error
 * @param response
 * @param xmlbody: an XML file
 */
function processRequestResults(error, response, xmlbody) {
    if (error) {
        throw (error);
    }

    xml2js.parseString(
        xmlbody,
        processXMLParseResult
    );
}

/**
 * Process each NGR metadata record
 * @param err
 * @param NGRxmljs
 */
function processXMLParseResult(err, NGRxmljs) {
    if (err) {
        console.error('Error: an error occurred while getting the main NGR metadata address ' + metadataURL);
        throw (err);
    }

    async.eachSeries(
        NGRxmljs.response.metadata,
        processNGRDataSet
    );
}

/**
 * Process a metadata record by creating a jekyll page for it, and go through the service links of the record
 * @param element a parsed XML2js object
 * @param callbackDone a callback function indicating to async.eachSeries the instruction has finished.
 */
function processNGRDataSet(element, callbackDone) {
    var wfsURL = undefined;

    element.link.forEach(
        function processLinkEntry(linkentry) {
            if (linkentry.split('|')[3] === "OGC:WFS") {
                wfsURL = linkentry.split('|')[2];
            }
        }
    );

    if (wfsURL) {
        var record = {
            title: element.title[0],
            tags: ngrtools.dashify(element.title[0]),
            description: element.abstract[0] + "\n" + "Documentatie: " + 'http://www.nationaalgeoregister.nl/geonetwork/resource/' + element['geonet:info'][0].uuid[0] + "\nServicelinks: " + JSON.stringify(generateServiceLinks(element.link)),
            wfsURL: wfsURL
        };

        ngrtools.saveMetadataRecord(record);
    } else {
        console.log('Warning: ' + element.title[0] + ' has no WFS link');
        console.log('Links: ' + JSON.stringify(element.links));
    }

/*
    async.eachSeries(
        element.link,
        processDataSetLinksSync
    );
*/

    callbackDone();
}

/**
 * Synchronous dataset function to prevent overloading the wfs2swagger spec generation script
 * @param element : an array element passed by async.eachSeries
 * @param callbackDone : a function passed by async.eachSeries as flagging for the next iteration to execute
 */
function processDataSetLinksSync(element, callbackDone) {
    processServiceLink(element);
    callbackDone();
}

/**
 * Inspects the service link type and creates a swagger spec if it is of type "OGC:WFS"
 * @param endpoint
 */
function processServiceLink(endpoint) {
    var servicetype = endpoint.split('|')[3]; // Mostly OGC:WFS and OGC:WMS
    //console.log('Processing endpoint description ' + endpoint);

    if (servicetype === "OGC:WFS") {
        var wfsendpoint = endpoint.split('|')[2];

        if (wfsendpoint == undefined) {
            console.log('Error: no endpoint extraction possible for endpoint description ' + endpoint);
            return;
        } else {
            var cleanwfsendpoint = ngrtools.cleanWFSendpoint(wfsendpoint); //The actual endpoint URI is often polluted with a '?' or request parameters
            var dashifiedWFStitle = ngrtools.titleify(wfsendpoint);
        }

        if (dashifiedWFStitle === undefined) {
            console.log('Error: no valid title generated for ' + wfsendpoint);
            return;
        }

        fs.exists(path.join(swaggerspecdir, dashifiedWFStitle + '.json'), function processSwaggerSpec(exists){
            if (!exists) {
                if (wfsEndpoints.indexOf(dashifiedWFStitle) != -1) {
                    console.log('Warning: swagger spec ' + dashifiedWFStitle + ' is already being processed by wfs2swagger.');
                } else {
                    wfsEndpoints.push(dashifiedWFStitle);

                    console.log('Info: requesting swagger specification for hitherto unprocessed endpoint ' + cleanwfsendpoint);

                    ngrtools.generateSwaggerSpec(cleanwfsendpoint, function processReturnedSwaggerSpec(spec) {
                        ngrtools.writeSwaggerSpec(dashifiedWFStitle + '.json', spec);
                        ngrtools.postToSwaggerHub(spec, dashifiedWFStitle);
                        addRecordsToFinalMetadata(cleanwfsendpoint);
                    });
                }
            } else {
                console.log('Info: skip generating specification for existing specification for endpoint ' + cleanwfsendpoint);
                addRecordsToFinalMetadata(cleanwfsendpoint);
            }
        });
    }
}
/**
 * generate web service and download links
 * @param links : an array of strings, with metadata separated by pipes '|'
 */
function generateServiceLinks(links) {
    var returnedlinks = [];

    links.forEach(
        function addLinkEntry(link) {
            var entry = {
                type: link.split('|')[3],
                url: link.split('|')[2]
            };
            returnedlinks.push(entry);
        }
    );

    return returnedlinks;
}

/**
 * Add metadata records vor a verified wfs endpoint
 * @param cleanendpoint : a verified clean wfs endpoint (no '?' or 'request=' business)
 */
function addRecordsToFinalMetadata(cleanendpoint){
	console.log('Info: searching for metadata records for verified endpoint ' + cleanendpoint);

	tempmetadata.datasets.forEach(function pushMetadata(record){
		if (ngrtools.cleanWFSendpoint(record.wfsURL) === cleanendpoint) {
            console.log('Info: found record ' + record.title + ' for ' + cleanendpoint);
            //TODO: request.post('http://data.test.pdok.nl/metadataapi',)
		}
	});
}
