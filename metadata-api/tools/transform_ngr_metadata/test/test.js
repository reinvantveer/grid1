'use strict';
var request = require('request');
var assert = require('assert');

var tools = require('../index.js');
var exampleWFSurl = 'https://geodata.nationaalgeoregister.nl/kweldervegetatie/wfs';
var exampleURL = 'https://www.google.com';
var kadasterURL = 'http://pdok-lod.in.kadaster.nl';

function sendRequest(URL) {
    request({
        method: 'POST',
        tunnel: false,
        proxy: null,
        url: URL
    }, function (error, response, body) {
        if (error){
            console.log(error);
        }
        console.log(body);
    });
}

function getProxyRequestBody(URL) {
    var responsebody = function () {
        request({
            method: 'POST',
            tunnel: false,
            proxy: 'http://ssl-proxy.kadaster.nl:8080',
            url: URL
        }, function (error, response, body) {
            if (error) {
                console.log(error);
            }
            return body;
        });
        return body;
    };

    console.log(responsebody);
}


//sendRequest(kadasterURL);
getProxyRequestBody(exampleURL);
// console.log(body);

//assert(getProxyRequestBody(exampleURL), 'proxy http request succeeded');