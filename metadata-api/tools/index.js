'use strict';
var config = require('./config.json');
var fs = require('fs');
var path = require('path');
var request = require('request');

module.exports.dashify = dashify;
module.exports.titleify = titleify;
module.exports.cleanWFSendpoint = cleanWFSendpoint;
module.exports.generateSwaggerSpec = generateSwaggerSpec;
module.exports.saveMetadataRecord = saveMetadataRecord;
module.exports.writeSwaggerSpec = writeSwaggerSpec;
module.exports.postToSwaggerHub = postToSwaggerHub;

/**
 * rewrite a string as readable, file-writeable string by replacing all non-alphanumerical characters with dashes
 * @param string
 * @returns string with alphanumerical signs and dashes
 */
function dashify(string) {
    if (string) {
        return string.replace(/\W+/g, '-'); //replace all non-alphanumeric characters with dashes
    }
}

function titleify(wfsEndpoint) {
    var cleanwfsendpoint = cleanWFSendpoint(wfsEndpoint);
    if (!cleanwfsendpoint) {
        console.error('Error: cleanWFSendpoint function did not return a cleaned endpoint URL for ' + wfsEndpoint);
        return;
    }
    var domain =  cleanwfsendpoint.split('/')[2];
    var dashifiedWFStitle = dashify(cleanwfsendpoint.split(domain + '/')[1]);

    if (!dashifiedWFStitle) {
        console.error('Error: no title to be generated for ' + wfsEndpoint);
        return
    }

    return dashifiedWFStitle.replace('-wfs', '')
        .replace('-MapServer-WFSServer', '')
        .replace('-WFS', '')
        .replace('_MapServer-WFSServer', '');
}

/**
 * Function for stripping off request parameters from base WFS endpoint URIs
 * @param dirtyendpointurl
 * @returns {*}
 */
function cleanWFSendpoint(dirtyendpointurl) {
    try {
        return dirtyendpointurl.split('?')[0];
    } catch (err) {
        console.error('Error splitting endpoint URL ' + dirtyendpointurl);
    }
}

/**
 * Request the generate service for generating a Swagger spec for a specific WFS endpoint
 * @param dataset
 * @param callback
 */
function generateSwaggerSpec(dataset, callback) {
    console.log('Processing swagger spec for wfs endpoint ' + dataset.wfsURL);

    request.post({
      url: config.wfs2swaggerURL,
      proxy: null,
        form: {
          wfs: dataset.wfsURL,
          version: '1.0',
          host: 'data.test.pdok.nl',
          basePath: '/apis/' + dataset.slug + '/v1.0'
        }
      }, handleSwaggerGenerateRequest(dataset, callback)
    );
}

function handleSwaggerGenerateRequest(dataset, callback){
    return function processResult(err, response, swaggerSpec) {
        if (err) {
            console.error(err);
            return;
        }

        //To prevent timeout premature script crashes
        if (!response) {
            console.error('Timeout');
            return;
        }

        if (response.statusCode != 200) {
            console.error("Post request failed with status code: " + response.statusCode);
            console.error("Response body: " + swaggerSpec);
            return;
        }

        var filename = dataset.slug + ".json";
        writeSwaggerSpec(filename, swaggerSpec);
        setTimeout(function(){
            initiateJSONldHarvest(dataset.slug);
        }, 30000);
        postToSwaggerHub(swaggerSpec, dataset);
        return callback(swaggerSpec);
    }
}

/**
 * Initiates harvest from json-ld to triple store
 * @param slug
 */
function initiateJSONldHarvest(slug) {
    console.log('Sent command to harvest http://data.test.pdok.nl/apis/' + slug + '/v1.0');
    request.get({
        url: config.queueManagerURL + slug,
        proxy: null,
        json: true
    }, function (err, response, body) {
        console.log('Got response from harvester');
        if (err) {
            return console.error(err);
        }
        if (response.statusCode == 202) {
            return console.log('Command accepted');
        }

        console.error(response);
    });
}

/**
 * Handles the result for saving a metadata record http post request
 * @param err
 * @param response
 * @param body
 */
function handleSavePostResult(err, response, body){
    if (err) {
        console.error(err);
    }

    //To prevent timeout premature script crashes
    if (!response) {
        return;
    }

    if (response.statusCode == 201) {
        console.log("Post request succeeded, dataset added!");
    } else {
        console.error("Post request failed with status code: " + response.statusCode);
        console.error("Response body: " + body);
    }
}


/**
 * generate a metadata record to add to a JSON file for digestion and/or render by front end
 * @param metadata
 * @returns {{title: *, date: Date, modified: Date, tags: *, description: *, swaggerlink: string, swaggerdoclink: *, servicelinks: *}}
 */
function saveMetadataRecord(metadata) {
    var formData = {
        title: metadata.title,
        tags: metadata.tags ? dashify(metadata.tags): "",
        description: metadata.description,
        wfsURL: metadata.wfsURL
    };

    request.post(
        {
        url: config.metadataSaveURL,
        form: formData
        }, handleSavePostResult
    );
}

/**
 * function for writing swagger specifications to a directory
 * @param filename : name for the file, sans directory
 * @param spec : swagger specification in JSON
 */
function writeSwaggerSpec(filename, spec){
    fs.writeFileSync(path.join(config.swaggerSpecDir, filename), spec);
    console.log('Wrote swagger specification for ' + filename);
}

/**
 * nodejs request for posting the swagger spec to swagger hub, this generates documentation for the API
 * @param spec : a Swagger specification
 * @param dataset : a dataset name
 */
function postToSwaggerHub(spec, dataset) {
    console.log('Posting ' + dataset.slug + 'to Swagger Hub');
    request({
        method: 'POST',
        tunnel: false,
        proxy: 'http://ssl-proxy.kadaster.nl:8080',
        no_proxy: 'localhost,*.pdok.nl,*.kadaster.nl,jekyll,',
        timeout: 60000,
        url: 'https://api.swaggerhub.com/apis/pdok/' + dataset.slug,
        headers: {
            'Content-Type': 'application/json',
            Authorization: 'eyJUb2tlblR5cGUiOiJBUEkiLCJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJhZGEwNjgyNi05MmY3LTRiMzAtYWVmZS1iNmFlYjU3YjgwZWIiLCJpYXQiOjE0NDY2NDk4MTN9.MTo1vgFhkGluuywczg2EbGi-nRVuuGiorREWDuYCpXa359XDjeDKlUtk19Fxhf6e7EEhhjoFVaijQUTLgsk8Ig'
        },
        body: spec
    }, function (error, response) {
        if (error){
            console.error('Error while posting to swagger hub: ' + error);
        }
        console.log('Posted ' + dataset.slug + ' specification to swagger hub, response: ' + JSON.stringify(response));
        console.log('Swagger Hub post response stats code: ' + response.statusCode + ' for ' + dataset.slug);
    });
}
