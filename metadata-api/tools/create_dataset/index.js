'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var wfs2swagger = require('../../wfs2swagger/index.js');

var app = express();
app.use(bodyParser());

app.post('/createdataset', createdataset);
/**
 * creates a dataset entry in a json file for a front end to digest and render
 * @param req form generated request
 * @param res express response object
 */
function createdataset(req, res) {
    var metadatafile = path.join(__dirname, '../jekyll/resources/appendedmetadata.json');
    var cleanWFSendpoint = wfs2swagger.cleanWFSendpoint(req.body.endpoint);
    var cleanWFSname = wfs2swagger.dashify(req.body.endpoint.split('?')[0]);

    var record = wfs2swagger.generateMetadataRecord({
        metadata: {
            title: req.body.title,
            tags: req.body.tags,
            description: req.body.description,
            swaggerlink: '/apis/' + cleanWFSname,
            wfsURL: req.body.wfsURL
        }
    });

    if (record) {
        fs.appendFileSync(metadatafile, record);
    }

    var filename = path.join(__dirname, '../../api/swagger/', cleanWFSname + '.json');

    var swaggerspec = wfs2swagger.generateSwaggerSpec(cleanWFSname, function(swaggerspec){

        if (!swaggerspec) {
            console.error('Error parsing endpoint ' + req.body.endpoint + ' to a swagger spec');
            res.send('Mislukt: ' + req.body.endpoint + ' kon niet worden omgezet in een Swagger specificatie');
        } else if (fs.existsSync(filename)) {
            console.error('Swagger spec ' + dashify(wfsEndpoint) + ' already exists.');
            res.send('Mislukt: ' + req.body.endpoint + ' kon niet worden omgezet in een Swagger specificatie');
            return;
        }

        fs.writeFileSync(filename, body);
        wfs2swagger.postToSwaggerHub(swaggerspec, cleanWFSname);
        res.send('Gelukt: ' + req.body.endpoint);

    });
}

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('dataset creation app listening at http://%s:%s', host, port);
});
