#!/usr/bin/env bash
# Create services script
# cp /opt/kadaster/nginx/deploy/default /etc/nginx/sites-enabled/default
# cp /opt/kadaster/nginx/nginx.conf /etc/nginx/nginx.conf
npm config set proxy http://ssl-proxy.kadaster.nl:8080
npm config set https-proxy https://ssl-proxy.kadaster.nl:8080
npm config set strict-ssl false
cd /opt/wfs2swagger && npm install
forever -a -w -l /opt/logs/wfs2swagger.log -o /opt/logs/wfs2swagger.out.log -e /opt/logs/wfs2swagger.error.log /opt/wfs2swagger/index.js
