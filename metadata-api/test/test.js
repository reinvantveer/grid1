var config = require ('../config.json');
var assert = require('assert');
var mongoose = require('mongoose');
var validator  = require('../models/dataset.js');
var fs = require('fs');
var path = require('path');
var dom = require('xmldom').DOMParser;

var nonHttpAddress = 'my-wrong-address';
var validWFSAddress = 'https://geodata.nationaalgeoregister.nl/mosselenoesterhabitats/wfs';
var getCapabilitiesRequestParam = '?request=getCapabilities&version=2.0.0';
var validWFSAddress2 = 'http://geodata.rivm.nl/geoserver/wfs';
var google = 'https://www.google.com';
var planbureauleefomgeving = 'http://geoservice.pbl.nl/arcgis/services/projecten/Nacht_Geluidcontouren_Schiphol_2004_2010/MapServer/WFSServer?request=GetCapabilities&service=WFS';
var fakedResponseBodyXML = fs.readFileSync(path.join(__dirname, '/mapserv-test-no-geojson.xml'), 'ascii');
var fakedPaginateErrorXML = fs.readFileSync(path.join(__dirname, '/getFeature-paginate-error.xml'), 'ascii');
var fakedResponseBodyDoc, fakedPaginateErrorDoc;

beforeEach(
    function(){
        fakedResponseBodyDoc = new dom().parseFromString(fakedResponseBodyXML);
        fakedPaginateErrorDoc = new dom().parseFromString(fakedPaginateErrorXML);
    }
);

describe('metadata-api', function tests() {

    this.timeout(30000);

    it('should be able to connect to MongoDB on ' + config.mongodbConnectString, function canConnect(done){
        assert.doesNotThrow(function connect(){
            mongoose.createConnection(config.mongodbConnectString, function verifyNoError(err){
                assert.equal(err, undefined);
                done();
            });
        });
    });

    it('should not be able to connect to some bogus connection ', function shouldnotConnect(done){
        assert.doesNotThrow(function connect(){
            mongoose.createConnection("useless", function verifyNoError(err){
                assert.notEqual(err, undefined);
                done();
            });
        });
    });

    it('should invalidate a non-http address', function invalidateNonHttp(done) {
        validator.isValidWFS(nonHttpAddress, function(isValid) {
            assert.equal(isValid, false);
            done();
        });
    });

    it('should reject endpoints with response type other than application/xml or text/xml', function verifyXML(done){
        validator.isValidWFS(google, function(isValid){
            assert.equal(isValid, false);
            done();
        });
    });

    it('should reject endpoints that do not have the 2.0.0 tag', function hasWFS200(done){
        assert.doesNotThrow(function checkWFS200(){
            validator.isValidWFS(planbureauleefomgeving, function(isValid){
                assert.equal(isValid, false);
                done();
            });
        });
    });

    it('should verify that the WFS endpoint has not thrown an exception report at us', function verifyHasNoException(){
        assert.equal(validator.hasExceptionReport(fakedResponseBodyDoc), false);
    });

    it('should verify that the WFS endpoint can detect an exception', function verifyHasNoException(){
        assert.equal(validator.hasExceptionReport(fakedPaginateErrorDoc), true);
    });

    it('should reject endpoints that are unable to deliver GeoJSON content type for features', function canDeliverGeoJSON(done){
        assert.doesNotThrow(function checkCapableOfGeoJSON(){
            var doesGeoJSON = validator.isCapableOfGeoJSON(fakedResponseBodyDoc);
            assert.equal(doesGeoJSON, false);
            done();
        });
    });

    it('should verify the WFS point is capable of pagination', function verifyPagination(done) {
        var isCapable = validator.isCapableOfPagination(validWFSAddress, 'mosselenoesterhabitats', function hasException(isCapable){
            assert.equal(isCapable, true);
            done();
        });
    });

    it('should validate a standard WFS 2.0 endpoint', function validateStandardWFS(done) {
        assert.doesNotThrow(function checkValidStandardWFS(){
            validator.isValidWFS(validWFSAddress2 + getCapabilitiesRequestParam, function verificationCallback(isValid){
                assert.equal(isValid, true);
                done();
            });
        });
    });
});

afterEach(function wrapUp(){
    mongoose.disconnect();
});