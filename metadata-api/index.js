'use strict';
var config = require('./config.json');
var mongoose = require('mongoose');
var express = require('express');
var request = require('request');
var Dataset = require('./models/dataset.js');
var bodyParser = require('body-parser');
var tools = require('./tools/index.js');
var app = express();

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(
  function(req, res, next) {
    res.set({
      'Acces-Control-Allow-Origin': '*',
      'Content-Type': 'application/hal+json'
    });
    next();
  }
);

app.get('/datasets', getAllDatasets);
app.get('/datasets/:id', getExistingDataset);
app.post('/datasets', addNewDataset);
app.put('/datasets/:id', updateExistingDataset);
app.delete('/datasets/:id', deleteExistingDataset);


mongoose.createConnection(config.mongodbConnectString, startListening);

function startListening(err){
    if (err){
        console.error('Could not establish connection to MongoDB on ' + config.mongodbConnectString + '. Is it installed and running?')
        process.exit(1);
    } else {
        var server = app.listen(3020, function () {
            var host = server.address().address;
            var port = server.address().port;

            console.log('Metadata api listening at http://%s:%s', host, port);
        });
    }
}

function getAllDatasets(req, res) {
  Dataset.find(function(err, datasets) {
    if (err) {
      console.error(err.stack);
      return res.send(err);
    }

    res.json({ _embedded: datasets });
  });
}

function getExistingDataset(req, res) {
  Dataset.findById(req.params.id, function(err, dataset) {
    if (err) {
      console.error(err.stack);
      return res.status(404).json(err);
    }

    res.json(dataset);
  });
}

function addNewDataset(req, res) {
  var dataset = new Dataset();

  dataset.title = req.body.title;
  dataset.tags = req.body.tags;
  dataset.description = req.body.description;
  dataset.wfsURL = req.body.wfsURL;
  dataset.graphURI = req.body.graphURI;

  dataset.save(function(err) {
    if (err) {
      console.error(err.stack);
      return res.status(400).json(err);
    }

    tools.generateSwaggerSpec(dataset,
      function(swaggerspec){
        if (!swaggerspec) {
            console.error('Error parsing endpoint ' + dataset.wfsURL + ' to a swagger spec');
            return res.status(400).json({ message: 'Error parsing endpoint ' + dataset.wfsURL + ' to a swagger spec' });
        }
      }
    );

    res.status(201).json({ message: 'Dataset added!', data: dataset });
  });
}

function updateExistingDataset(req, res) {
  Dataset.findById(req.params.id, function(err, dataset) {
    if (err) {
      console.error(err.stack);
      return res.send(err);
    }

    dataset.title = req.body.title;
    dataset.tags = req.body.tags;
    dataset.description = req.body.description;
    dataset.wfsURL = req.body.wfsURL;
    dataset.graphURI = req.body.graphURI;

    dataset.save(function(err) {
      if (err) {
        console.error(err.stack);
        return res.send(err);
      }

      res.json(dataset);
    });
  });
}

function deleteExistingDataset(req, res) {
  Dataset.findByIdAndRemove(req.params.id, function(err) {
    if (err) {
      console.error(err.stack);
      return res.send(err);
    }

    res.json({ message: 'Dataset removed!' });
  });
}
